@isTest
Private Class MyFirstTestClass{
    static testMethod void validateMyFirstClass(){
    book__c b=new book__C(name='My First Book',price__c=100);
    system.debug('before inserting the book' +b.price__C);
    insert b;
    b=[select price__c from book__c where id=:b.id];
    system.debug('after inserting the book' +b.price__c);
    system.assertEquals(90,b.price__C);
    }
}